# README #
Este repositorio contiene los planos de las piezas que se han realizado para la fabricación de la cápsula de las sillas de ruedas 
basket en silla. Esta cápsula permite al jugador, tener un soporte para los pies protegidos sin que sea necesario cincharlos.

La cápsula inicialmente está pensada para construirse con 5 piezas de plástico y unas pletinas de aluminio de 3mm para añadir 
rigidez al diseño, todo ello se fija con tornillos de diferentes longitudes ISO 7380 de inoxidable que sirven tanto para fijar las pletinas como el conjunto 
a la silla. 

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact